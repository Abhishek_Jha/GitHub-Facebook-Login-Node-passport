module.exports = {
  // App Settings
  MONGO_URI: process.env.MONGO_URI || 'localhost',
  TOKEN_SECRET: process.env.TOKEN_SECRET || 'secret@1234567890!@#$%^&*',

  // OAuth 2.0
  FACEBOOK_SECRET: process.env.FACEBOOK_SECRET || '84e952da26fc9e7a12bbc32523efa8a0',
  GITHUB_SECRET: process.env.GITHUB_SECRET || 'c4fca852b13fa6e432b1611078e626555281602b'
 

 
  
};
